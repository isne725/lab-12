#include<iostream>
#include<string>
#include<list>
using namespace std;

//Class for stored data.
class Data
{
public:
	void setID(int newID);
	void setName(string newName);
	int getID();
	string getName();
private:
	long long id;
	string name;
};

//Prototype
int hashID(int id); //set index 's studentID of array that stored data.
int hashName(string name); //set index 's student name of array that stored data.
void data(long long id, list<Data> StudentID[50], list<Data> StudentName[50], int isne); //Input data of student's ISNE.
void ISNE(list<Data> StudentID[50], list<Data> StudentName[50]); //Stored data of student's ISNE.
void searchByID(list<Data> StudentID[50]); //Searching the student's information from studentID.
void searchByName(list<Data> StudentName[50]); //Searching the student's information from student name.

//Main
int main()
{
	int choice,loop=1;
	list<Data> StudentID[50]; //list for stored data by hash ID.
	list<Data> StudentName[50]; //list for stored data by hash name.
	ISNE(StudentID, StudentName); //call function that stored data of all ISNE.
	
	cout << "\n\tISNE 's Information\n"; 

	while (loop!=0)
	{	
		//Choice manu.
		cout << "_____________________________________\n";
		cout << "\nChoice 1 : Search by student's ID" << endl;
		cout << "\nChoice 2 : Search by student's name" << endl;
		cout << "\nChoice 0 : End Program" << endl;
		cout << "\n-------------------------------------\n" << "Enter choice that provide: ";
		cin >> choice;

		switch (choice)
		{
		case 1:
			searchByID(StudentID); //call  function that searching the student's information from studentID.
			break;
		case 2:
			searchByName(StudentName); //call  function that searching the student's information from student name.
			break;
		case 0:
			loop=0;
			break;
		default:
			cout << "Invalid value,please input again"<< endl;
			break;
		}
	}
	cout << "End Program" << endl;
	return 0;
}

//Function in class.
void Data::setID(int newID)
{
	id = newID;
}

void Data::setName(string newName)
{
	name = newName;
}

int Data::getID()
{
	return id;
}

string Data::getName()
{
	return name;
}

//Function
//set index 's studentID of array that stored data.
int hashID(int id) 
{
	return id % 50;
}

//set index 's student name of array that stored data.
int hashName(string name)
{
	int sum = 0;
	for (int i = 0; i < name.size(); i++)
	{
		sum = sum + name[i];
	}
	return sum % 50;
}

//Input data of student's ISNE.
void data(long long id, list<Data> StudentID[50], list<Data> StudentName[50], int isne)
{
	Data info;
	string name = to_string(isne);
	info.setID(id);
	info.setName(name);
	StudentID[hashID(id)].push_back(info);
	StudentName[hashName(name)].push_back(info);
}

//Stored data of student's ISNE.
void ISNE(list<Data> StudentID[50], list<Data> StudentName[50])
{
	int isne=1;

	//ISNE#3
	for (long long id = 580611001; id <= 580611050; id++)
	{
		data(id, StudentID, StudentName, isne);
		isne++;
	}
	//ISNE#4
	for (long long id = 590611001; id <= 590611050; id++)
	{
		data(id, StudentID, StudentName, isne);
		isne++;
	}
	//ISNE#5
	for (long long id = 600611001; id <= 600611050; id++)
	{
		data(id, StudentID, StudentName, isne);
		isne++;
	}
	//ISNE#6
	for (long long id = 610615001; id <= 610615050; id++)
	{
		data(id, StudentID, StudentName, isne);
		isne++;
	}
	//ISNE#7
	for (long long id = 620615001; id <= 620615050; id++)
	{
		data(id, StudentID, StudentName, isne);
		isne++;
	}
}

//Searching the student's information from studentID.
void searchByID(list<Data> StudentID[50])
{
	long long id;
	cout << "Enter the StudentID: ";
	cin >> id; //Input studentID from user.

	list<Data>::iterator search = StudentID[hashID(id)].begin();

	while (search != StudentID[hashID(id)].end())
	{
		if (id == search->getID())
		{
			cout << "Student 's Name: " << search->getName() << endl; //Displays the student name that user searching.
		}
		search++; 
	}
}

//Searching the student's information from student name.
void searchByName(list<Data> StudentName[50])
{
	string name;
	cout << "Enter the Student's Name: ";
	cin >> name; // Input student name from user.

	list<Data>::iterator search = StudentName[hashName(name)].begin();

	while (search != StudentName[hashName(name)].end())
	{
		if (name == search->getName())
		{
			cout << "Student ID: " << search->getID() << endl; //Displays the studentID that user searching.
		}
		search++;
	}
}
